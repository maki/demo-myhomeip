import json
import boto3
import os
ec2 = boto3.resource('ec2')
security_group = ec2.SecurityGroup('')
client = boto3.client('ec2')

"""
Params:
    str username: username of the person who is creating the ip
    str ip: ip address to add
"""
def lambda_handler(event, context):
    username = event["username"]
    ip1 = event['ip']
    ip1 += "/32"
    ListOfSecurityGroupIds = os.environ['SecurityGroupId_Env_Var'].split(" ")
    response = client.describe_security_groups(
        GroupIds = ListOfSecurityGroupIds
    )

    #remove other ips from the same person
    for SecurityGroup in response['SecurityGroups']:
        if SecurityGroup['IpPermissions']:
            for ip in SecurityGroup['IpPermissions'][0]['IpRanges']:
                if (ip['Description'] == username):
                    security_group.revoke_ingress(
                        CidrIp = ip['CidrIp'],
                        FromPort = 0,
                        GroupId = SecurityGroup['GroupId'],
                        IpProtocol = 'tcp',
                        ToPort = 65535
                    )

    #add new ip to each security group
    for SecurityGroup in ListOfSecurityGroupIds:
        try:
            security_group.authorize_ingress(
                IpPermissions = [{
                    'IpRanges': [{
                        'CidrIp' : ip1,
                        'Description' : username
                    }],
                'FromPort' : 0,
                'ToPort' : 65535,
                'IpProtocol' : 'tcp'
                }],
                GroupId = SecurityGroup
            )
        except:
            return {
                'statusCode': 409 ##Duplicate rule already present.
            }
    return {
        'statusCode': 200
    }