This repo has demo code to accompany the talk A mechanism for tracking employee IP addresses in a Security Group

The .html file is expected to be served from a web server or a static site bucket

The .py file is to be pasted into a lambda function

You will also need Cognito Identity Pool created and connected to your IDP(Id provider).  The identiy pool needs a role that the user will assume so that role needs to be able to invoke the target lambda.  See below for an example.

```JavaScript
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "Lambda:InvokeFunction",
            "Resource": [
                "arn:aws:lambda:us-east-1:828807864627:function:myhomeip",
                "arn:aws:lambda:us-west-2:828807864627:function:myhomeip"
            ]
        }
    ]
}
```